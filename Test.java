//Test code for filter
// args[0] : path to txt file with toxic comments
// args[1] : path to csv file with non toxic comments, text in first columns


import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;

//Results on STT hate speech 5942 samplés, VFe34 intent 63719 samples
// TP Score > 0: 69.53887
// TP Score > 1: 65.01178
// TP Score > 2: 63.598114
// 1697ms total over 5942 samples
// FP-Score > 0: 0.11927369
// FP Score > 1: 0.06277563

public class Test {

    public void checkFileToxic(String path){
        try {
            float doc_len = 0;
            float count_1 =0;
            float count_2 =0;
            float count_3 =0;
            int score;
            RuleBaseFilter obj = new RuleBaseFilter();
            String[] data= {};
            File inputFile = new File(path);
            Scanner fileReader = new Scanner(inputFile);
            while (fileReader.hasNextLine()) {
                String line = fileReader.nextLine();
                doc_len+=1;
                score = obj.filter(line, false)[6];
                if (score > 0) {
                    count_1+=1;
                }
                if (score > 1) {
                    count_2+=1;
                }
                if (score > 2) {
                    count_3+=1;
                }    
            }
            System.out.println("TP Score > 0: "+String.valueOf(count_1/doc_len*100));
            System.out.println("TP Score > 1: "+String.valueOf(count_2/doc_len*100));
            System.out.println("TP Score > 2: "+String.valueOf(count_3/doc_len*100));
            fileReader.close();
        }   catch (FileNotFoundException e) {
                System.out.println("An error occurred.");
                e.printStackTrace();
        }
    }

    public void checkFileClean(String path){
        try {
            float count_1 =0;
            float count_2 =0;
            float doc_len = 0;
            int score;
            RuleBaseFilter obj = new RuleBaseFilter();
            String[] data= {};
            File inputFile = new File(path);
            Scanner fileReader = new Scanner(inputFile);
            while (fileReader.hasNextLine()) {
                doc_len+=1;
                String line = fileReader.nextLine();
                line = line.split("\t")[0];
                score = obj.filter(line, false)[6];
                if (score > 0) {
                    count_1+=1;
                }
                if (score > 1) {
                    count_2+=1;
                }
    
            }
            System.out.println("FP-Score > 0: "+String.valueOf(count_1/doc_len*100));
            System.out.println("FP Score > 1: "+String.valueOf(count_2/doc_len*100));
            fileReader.close();
        }   catch (FileNotFoundException e) {
                System.out.println("An error occurred.");
                e.printStackTrace();
        }

    }


    public void terminalTest(){
        RuleBaseFilter obj = new RuleBaseFilter();
        String test;
        Scanner in = new Scanner(System.in);
        String[] name ={"Toxicity","Obscence","Threat","Insults","Sexual","Sedition","Spam"};
        while (true){
            System.out.println("Nhập câu nói ");
            test = in.nextLine();
            System.out.println(test);
            int[] res = obj.filter(test+' ', true);
            for (int i = 0; i < res.length; i++){
                System.out.print(name[i]+":  ");
                System.out.println(res[i]);
            } 
        }
    }
    public static void main(String[] args) {
        Test testObj = new Test();
        // long start = System.nanoTime();
        // testObj.checkFileToxic(args[0]);
        // long stop = System.nanoTime();
        // System.out.print((stop - start)/1_000_000);
        // System.out.println("ms total");

        // start = System.nanoTime();
        // testObj.checkFileClean(args[1]);
        // stop = System.nanoTime();
        // System.out.print((stop - start)/1_000_000);
        // System.out.println("ms total");
        testObj.terminalTest();
        
        
        
    }
}