// Regex filter for bad intent speech
// Input : all lower-case String
// Out: int arr, corresponds to how many patterns are matched and the pattern groups.
// The last score is the weighted sum of the numbers of patterns.
// [Toxicity    Obscence    Threat  Insult  Sexual-explicit    Sedition-politics    Spam]
import java.util.regex.*;


public class RuleBaseFilter {
    String any_word="(\\s[^\\s]*\\s)";
    String toxic_pronoun = "(tao|mày|thằng|con (kia|đấy))";
    String sexual_word_tier1 ="(chịch|nện|phang|xoạc)";
    String sexual_word_tier2 ="(húc|húp|chơi)";
    String sexual_content = "(lén lút|vụng trộm|cấp ba|loạn luân)";
    String violent_word = "(đánh|thiến|giã|đấm|tát|chém|tử hình|giết|phang|táng|bắn bỏ|vả|treo cổ|phanh thây|xử tử|xử bắn|đập)";
    String[] pattern_obs_1 = {
        "đờ cờ mờ ",
        "đụ ",
        "địt",
        "đéo",
        "buồi",
        "đầu bùi",
        "đờ mờ",
        "lồn ",
        "ăn\\slòn\\s",
        "đù\\smá",
        "cặc",
        "đệt",
        "con\\scu ",
        "như\\s(chó|bò|lợn|heo|lòn|cái lòn|cứt|cức) ",
        "sấp\\smặt\\slờ",
        "xạo\\s(lìn|lờ)",
        "mẹ\\s(tao|mày)",
        "chết\\s(mẹ|cha|cụ|ông|bà|chó|mịa)",
        "(con|thằng)\\s(mẹ|cha|cụ|ông|bà|chị) ",
        "hãm\\s(vãi|lờ)",
        "(con|dòng|thằng)\\s(đĩ|điếm|phò)",
        "(vãi|mặt)\\s(.*?\\s)?(lờ|lìn|lòi|cờ|đái|cứt|cức)",
        "(con|thằng|đồ|thứ)\\sóc\\schó",
        "cả\\slòn? ",
        "(bố|mẹ|cụ)\\s(.{0,20}\\s)?mày",
        "bỏ\\s(mẹ|bố)",
        "bullshit",
        "fuck",
        "bitch",
        "tao.{0,15}(cứt|cức)|(cức|cứt).{0,15}tao",
        "chơi chó",
        "(?<!(cây|quả|hạt))(\\s|^)óc\\schó",
        "ăn\\s(cứt|cức|phân)",
        "(thằng|con)\\sđầu\\s(bò|heo|lợn|chó)",
        "cục\\s(cức|cứt)",
        "(mày.*sủa|sủa.*mày)",
        "mày.*con\\schó|con\\schó.*mày",
        "(mờ|cái|vờ)\\slờ",
        "mả\\s(mẹ|tổ)",
        "chặt\\scu",
        "tiên\\ssư\\s(cha|bố)",
        "toạc\\slờ",
        "\\séo\\s(ai|hiểu|dám)",
        "bôn\\slành",
        "khùng\\slờ",
        "(bố|mẹ)\\s((con|thằng)\\s)điên",
        "chuẩn mẹ nó rồi",
        "đíu có",
        "thằng chó",
        "rảnh vồn",
        "cái động chó",
        "im mẹ",
        "(lỗ|đau|ngứa)\\sđít",
        "tung lòn",
        "\\séo làm",
        "đù\\s[\\.,!?]",
        "\\sẳng\\s(to)?thế ",
        "kệ (bố|mẹ|bà|cụ|ông) (tao|mày|nó)",
        " cút ",
        "kệ (mẹ|bố|bà|cụ)",
        "(chê|bốc) cứt",
        "rơi mõm ",
        "hà thiên lộn",
        "đĩ mẹ "
    };
    String[] pattern_obs_2 = {
        " ỉa",
        "khóa mõm",
        "(con|thằng) đấy",
        "chúng mày (?!ơi)(?!ạ)"
    };
    String[] pattern_se = {
        "nứng",
        "lồn ",
        "cặc",
        "phim (sex(?!y)|người lớn)",
        "x ?x ?x",
        "chat (x ?x|se )",
        "chịch",
        "vếu",
        "xoạc",
        "(?<!hai\\s)bím\\s(?!tóc)",
        "đi khách",
        "show (hàng|chéo)",
        "(khoe|check) hàng",
        "đủ mười tám (chưa|rồi)",
        "thủ dâm",
        "dâm (?!(bụt))",
        "ảnh nude",
        "bướm.{0,10} hư ",
        "em hot girl",
        "móc cua",
        "(bóp|sờ) (vú|vếu|cu|ngực|đít|mông)",
        "(vú|vếu|ngực|mông).{0,10}(đẹp|to|bự|nhỏ)",
        " ti thâm",
        "tung clip",
        "mười tám cộng",
        "nện",
        "ngọt nước",
        "hấp diêm",
        "chơi .{0,10}(les|bê đê|lét|gái|đàn ông|cave)",
        "chim (tao|mày) (nhỏ|bé)",
        sexual_word_tier2+" (cả đêm|suốt đêm)",
        "(khóc thét|quằn quại).{0,10}vì sướng",
        "clip.*không che",
        "bạn tình một đêm",
        "chơi (lỗ nhị|lỗ đít)",
        "bú .{0,8}liếm",
        "em gái.*bạn trai",
        sexual_word_tier2+" (tao|mày)",
        "cu .{0,10}mồm",
        "clip "+sexual_content,
        "giao lưu tình dục",
        "chăn rau",
        "(còn|gái|phá) trinh",
        " em rau ",
        "chim.*cong ",
        "vú bướm",
        "(thịt|thịch) (em|bé) ",
        "sờ bướm",
        "(cu|bướm) giả",
        "(chim|cu) ngắn",
        "video .*vét máng ",
        "(cởi|tụt) (đồ|quần|áo) ra",
        "làm tình.*nhiệt tình",
        "(bú|liếm|thổi) .{0,10} (em|anh) ",
        "lỗ nhị ",
        "bú cu ",
        "(xem|show) (cu|bướm) ",
        "trái dứng ",
        "mấy nháy",
        "nghe rên ",
        "muốn .{0,20} giải quyết sinh lý",
        "tìm (anh trai nuôi|em gái nuôi|sugar babdy|sugar daddy)",
        " nắc ",
        "khít.* hồng| hồng.* khít",
        "ngủ không (anh|em)",
        "ngủ với (anh|em) nhé",
        "húp (luôn|như)",
        "phim (con )?heo",
        "tét (mông|đít|bướm)",
        "full clip .* phút",
        "khe (vú|vếu) ",
        "kẹp chim ",
        "đá phò",
        "(cu|chim).{0,10}cương ",
        "lếu lều"

        

    };
    String[] pattern_ins_1= {
        "tốn (cơm|gạo)",
        "bằng .{0,10}(chó|lợn|bò)",
        "đần (độn)?",
        "mày.*chui cống rãnh",
        "tinh trùng khuyết tật",
        "phò ",
        "dở người ",
        "sân si ",
        "cẩu nam (cẩu )?nữ ",
        "(bọn|thằng) (chó|thượng đẳng|súc vật|súc sinh) ",
        "giả tạo ",
        "đấm (vào|vô) (đít|tai)",
        "bị (đa nhân cách) ",
        "(thằng|con) (cuồng|quái thai)",
        "(hơn|bằng|kém|thứ|đồ|loại|thằng|con) .{0,10}súc vật",
        "(mày|^) (có )?bị .{2,20} (à|không)",
        "teo dái",
        "vô (liêm sỉ|học)",
        "dốt nát",
        "(thằng|con) đàn bà",
        "thằng mặc váy",
        "dưới đít"
    };
    String[] pattern_ins_2 ={
        "(?<!(song))\\sngu\\s",
        "lươn lẹo",
        "điên",
        "dở hơi",
        "thiểu năng"
    };
    String[] pattern_sed ={
        "đmcs",
        "bò đỏ",
        "dư luận viên",
        "(cộng sản|chế độ|chxhcs|csvn|chxhcn|chxhvn).{0,30} (ác bá|bán nước|trộm cắp|trộm cướp|tham quan|hối lộ|tham tiền|bất công|nghèo đói|độc tài|thối nát| thúi nát)",
        "(chống lại|sụp đổ|tiêu diệt|lật đổ) .{0,20} cộng sản",
        "(cộng sản|chế độ|xã hội chủ nghĩa) .{0,20}(?<!(không)).{0,10}(sụp đổ|tan rã|diệt vong|nát|tiêu diệt)",
        "(chxh|chủ nghĩa).{0,20}chết hết",
        "trung cộng",
        "việt cộng",
        "(bác hồ|cụ hồ|thằng hồ).{0,20} chết",
        "việt nam .{0,15} thối nát",
        "(thằng|đồ) bán nước",
        "bọn (dân chủ|cộng sản) ",
        "đảng (thổ tả)",
        "thằng tàu",
        "thằng (võ nguyên giáp|hồ chí minh)",
        "bản chất .{0,10}mị dân",
        violent_word+".{0,10} cộng sản",
        "tàu khựa",
        "con ma họ Hồ",
        "(tẩy chay|đả đảo|lật đổ) .{0,10}chủ nghĩa xã hội",
        "trọng lú"
    };
    String[] pattern_threat={
        violent_word+" chết",
        "chém (bay đầu|nhau|giết)",
        "(đánh|đấm) nhau (với tao )?không",
        violent_word+"\\s(nó|mày|tao|chúng nó)(\\schết)?",
        "(giã|phang|đấm|sút|đá).{0,15}vào (mồm|đầu|mặt)",
        "(?<!(nắm ))(cắt|chặt) (trym|cu|chân|tay|đầu|chim)",
        violent_word+" thoải mái",
        "(cho|là) ((mày|nó) )?xanh cỏ",
        "tao.{0,15}xanh cỏ",
        "ngon (nhào|thì) (vô|dô)",
        "(bắn|tử hình|chém|đấm|táng) (luôn|cho)",
        "(đánh|đốt|đập) cho nó",
        "(chết|cẩn thận) với tao",
        "(nhớ mặt) tao",
        "\\snha con ",
        "(đấm|đá|tát|vả|đạp) cho ((một|vài|mấy) )?(cái|phát)",
        "đập đầu mày",
        "thì xác định đi",
        "(cho|đập) (cây gậy|dùi cui) vào (mặt)",
        "kí cái đầu",
        "một (sút|đạp) là (xong|xuống|bay)",
        "cho (nó|mày) (chết|rục xương)",
        "(đấm|đập|giã) (cho )?(vỡ )?(mồm|mặt)",
        "(trét (cức|cứt)|vả) vào (mặt|mồm)",
        "tao"+any_word+"?"+violent_word,
        "vác dao",
        "cái hẹn.*dám",
        "cho (xin|tao|tao xin) cái hẹn",
        "bố láo",
        "xin cái địa chỉ",
        "thì (kinh|ghê) (rồi|quá)",
        "đánh sập.*đi",
        "ăn (táng|đấm|vả|sút|đạp)",
        "ăn (một|vài) (cái|phát) (vả)",
        "lo mà (trốn|chạy) đi",
        "(đánh|đấm|vả |tát).*rụng răng",
        "không (trốn|thoát|trốn thoát) được đâu",
        "vote (bắn|tử hình|bỏ tù)",
        "(chết|tự thiêu|tự tử|tự sát|lột sạch) đi",
        "(đánh) cho nhấc người",
        "(giết|tiêu diệt) hết.*đi",
        "đừng để (tao|bố) bắt được",
        "cho (chục|vài) nhát",
        "cho viên gạch vào",
        "không trượt phát nào",
        "(đánh|hiếp) đến chết",
        "cho .*? treo cổ",
        "quăng (bom|lựu đạn).*nó",
        "cho nó (một )?(viên|nhát) (chì|kẹo đồng|đạn|gạch|kiếm|dao)",
        "xin (nó|mày )?tí (huyết|máu|tiết)",
        "sút (bọn|tụi|đám|đống).*đi",
        "chó.{0,20}chết",
        "phải "+violent_word+".*thôi",
        violent_word+".* mẹ nó",
        "bố (mày )?cho mày",
        violent_word+" cho (chứ|đấy)",
        "thì "+violent_word+" hết",
        "đấm.*rụng răng",
        "phải (bị )?(treo cổ|tử hình|bắn bỏ|xử bắn|xử tử|ở tù)",
        "cho ra pháp trường",
        violent_word+ " (đứa nào|mấy đứa)",
        "nên "+violent_word+" ",
        "sút chết"
        
        
    };

    public int[] filter(String input, boolean print_pat) {
        int[] ret = {0,0,0,0,0,0,0};
        input = input +' ';
        for (int i = 0; i < this.pattern_obs_1.length; i++){
            Pattern pat = Pattern.compile(pattern_obs_1[i]);
            Matcher m = pat.matcher(input);
            if (m.find()) {
                if (print_pat){
                    System.out.println(pattern_obs_1[i]);
                }
                
                ret[1] = 1;
            }
        }
        for (int i = 0; i < this.pattern_obs_2.length; i++){
            Pattern pat = Pattern.compile(pattern_obs_2[i]);
            Matcher m = pat.matcher(input);
            if (m.find()) {
                if (print_pat) {
                    System.out.println(pattern_obs_2[i]);
                }
                
                ret[1] = 1;
            }
        }
        for (int i = 0; i < this.pattern_se.length; i++){
            Pattern pat = Pattern.compile(pattern_se[i]);
            Matcher m = pat.matcher(input);
            if (m.find()) {
                if (print_pat) {
                    System.out.println(pattern_se[i]);
                }
                
                ret[4] = 1;
            }
        }
        for (int i = 0; i < this.pattern_threat.length; i++){
            Pattern pat = Pattern.compile(pattern_threat[i]);
            Matcher m = pat.matcher(input);
            if (m.find()) {
                if (print_pat){
                    System.out.println(pattern_threat[i]);
                }
                
                ret[2] = 1;
            }
        }
        for (int i = 0; i < this.pattern_sed.length; i++){
            Pattern pat = Pattern.compile(pattern_sed[i]);
            Matcher m = pat.matcher(input);
            if (m.find()) {
                if (print_pat) {
                    System.out.println(pattern_sed[i]);
                }
                ret[5] = 1;
                
            }
        }
        for (int i = 0; i < this.pattern_ins_1.length; i++){
            Pattern pat = Pattern.compile(pattern_ins_1[i]);
            Matcher m = pat.matcher(input);
            if (m.find()) {
                if (print_pat) {
                    System.out.println(pattern_ins_1[i]);
                }
                ret[3] = 1;
            }
        }
        for (int i = 0; i < this.pattern_ins_2.length; i++){
            Pattern pat = Pattern.compile(pattern_ins_2[i]);
            Matcher m = pat.matcher(input);
            if (m.find()) {
                if (print_pat) {
                    System.out.println(pattern_ins_2[i]);
                }
                ret[3] += 0;
            }
        }
        return ret;

        
    }


}